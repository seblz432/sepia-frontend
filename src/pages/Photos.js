import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonText, IonTabButton, IonIcon, IonLabel, IonTabBar } from '@ionic/react';
import { search, personOutline, imageOutline } from 'ionicons/icons'
import styles from './Photos.module.css';

const Photos: React.FC = () => {
  return (
    <IonPage>
      {/*<IonHeader>
        <IonToolbar>
          <IonTitle>Sepia</IonTitle>
        </IonToolbar>
      </IonHeader>*/}

      <IonContent fullscreen>
        <h1 className={styles.title}>Sepia</h1>

        <div className={styles.content}>
          <div className={styles.dayOfPhotos}>
            <IonText color="black">
              <span>Today</span>
            </IonText>

        		<div className={styles.photos}>
              <div className={styles.skeletonPhoto} />
              <div className={styles.skeletonPhoto} />
              <div className={styles.skeletonPhoto} />
              <div className={styles.skeletonPhoto} />
            </div>
          </div>

          <div className={styles.dayOfPhotos}>
            <IonText color="black">
              <span>Yesterday</span>
            </IonText>

        		<div className={styles.photos}>
              <div className={styles.skeletonPhoto} />
            </div>
          </div>

          <div className={styles.dayOfPhotos}>
            <IonText color="black">
              <span>Sun, Feb 14</span>
            </IonText>

        		<div className={styles.photos}>
              <div className={styles.skeletonPhoto} />
              <div className={styles.skeletonPhoto} />
              <div className={styles.skeletonPhoto} />
              <div className={styles.skeletonPhoto} />
              <div className={styles.skeletonPhoto} />
              <div className={styles.skeletonPhoto} />
              <div className={styles.skeletonPhoto} />
            </div>
          </div>

          <div className={styles.dayOfPhotos}>
            <IonText color="black">
              <span>Sun, Feb 13</span>
            </IonText>

        		<div className={styles.photos}>
              <div className={styles.skeletonPhoto} />
              <div className={styles.skeletonPhoto} />
              <div className={styles.skeletonPhoto} />
              <div className={styles.skeletonPhoto} />
              <div className={styles.skeletonPhoto} />
              <div className={styles.skeletonPhoto} />
              <div className={styles.skeletonPhoto} />
              <div className={styles.skeletonPhoto} />
            </div>
          </div>
        </div>
      </IonContent>

      <IonTabBar slot="bottom" style={{border: 'none'}}>
        <IonTabButton tab="photos" href="/photos">
          <IonIcon icon={imageOutline} />
          <IonLabel>Photos</IonLabel>
        </IonTabButton>
        <IonTabButton tab="tab2" href="/search">
          <IonIcon icon={search} />
          <IonLabel>Search</IonLabel>
        </IonTabButton>
        <IonTabButton tab="account" href="/account">
          <IonIcon icon={personOutline} />
          <IonLabel>Account</IonLabel>
        </IonTabButton>
      </IonTabBar>
    </IonPage>
  );
};

export default Photos;
