import { IonContent, IonPage, IonTitle, IonToolbar, IonButton, IonText, IonInput, IonItem, IonLabel, IonIcon, IonRouterLink, IonLoading , IonAlert} from '@ionic/react';
import { informationCircleOutline } from 'ionicons/icons';
import React, { useState, useRef } from 'react';
import { useHistory } from 'react-router-dom';
import { v4 as uuidv4 } from 'uuid';

import styles from './Signup.module.css';

const Signup: React.FC = () => {
  const history = useHistory();

  const [showLoading, setShowLoading] = useState(false);
  const [showAlert, setShowAlert] = useState(false);

  const [message, setMessage] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  let deviceID = localStorage.getItem('deviceID');
  let userID = localStorage.getItem('userID');

  if (!deviceID) {
    deviceID = uuidv4();
    localStorage.setItem('deviceID', deviceID);
  }

  const handleFormSubmit = async function () {
    if (username == "") {
      setMessage('Missing username');
      setShowAlert(true);
      return;
    }
    if (password == "") {
      setMessage('Missing password');
      setShowAlert(true);
      return;
    }

    setShowLoading(true);

    // Generate new deviceID if a new user is being signed in to avoid duplicate deviceIDs
    if (userID != username) {
      userID = username;
      localStorage.setItem('userID', userID);

      deviceID = uuidv4();
      localStorage.setItem('deviceID', deviceID);
    }

  	var bcrypt = require('bcryptjs');
  	var salt = bcrypt.genSaltSync(10);
  	var saltSize = 30
  	salt = salt.substring(salt.indexOf('$'),salt.lastIndexOf('$')+1);
  	while(salt.length != saltSize)
  	{
  		if(salt.length<saltSize)
  		{
  			salt=salt+username;
  		}
  		else if(salt.length>saltSize)
  		{
  			salt=salt.substring(0,saltSize);
  		}
  	}
  	var hashPassword = bcrypt.hashSync(password, salt);

    var body = JSON.stringify({
      username: username,
      password: hashPassword,
      deviceID: deviceID
    });

    var requestOptions = {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json'
      }),
      body: body
    };

    let res = await fetch("https://api.sepia.co/signup", requestOptions);
    console.log(deviceID);
    console.log(res);

    if (res.ok) {
      setShowLoading(false);
      localStorage.setItem('user', username);
      history.push('/photos');
    } else {
      let parsedRes = await res.json();
      setShowLoading(false);

      //capitalize first letter
      setMessage(parsedRes.message.charAt(0).toUpperCase() + parsedRes.message.slice(1));
      setShowAlert(true);
    }
  }

  const emailRef = useRef(null);
  const passwordRef = useRef(null);

  const handleUsernameSubmit = async (e) => {
    if (e.key === 'Enter') {
      if (username != "") {
        emailRef.current.focus();
      } else {
        setMessage('Missing username');
        setShowAlert(true);
      }
    }
  }

  const handleEmailSubmit = (e) => {
    if (e.key === 'Enter') {
      passwordRef.current.focus();
    }
  }

  const handlePasswordSubmit = (e) => {
    if (e.key === 'Enter') {
      handleFormSubmit();
    }
  }

  return (
    <IonPage>
      <IonContent fullscreen>
        <div className={styles.titleSection}>
          <IonText color="dark">
            <h1>Create account</h1>
          </IonText>
        </div>

        <div className={styles.contentWrapper}>
          <div className={styles.content}>
            <input className={styles.input} placeholder="Username" value={username} onChange={e => setUsername(e.target.value)} onKeyUp={handleUsernameSubmit} type="text" />

            <input className={styles.input} placeholder="Email (optional)" value={email} onChange={e => setEmail(e.target.value)} onKeyUp={handleEmailSubmit} ref={emailRef} type="text" />

            <input className={styles.input} placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} onKeyUp={handlePasswordSubmit} ref={passwordRef} type="password" />
            <IonText className={styles.passwordSubtext} color="medium"><IonIcon className={styles.passwordSubIcon} icon={informationCircleOutline} /> Should contain at least 8 characters</IonText>

            <IonButton className={styles.button} type="submit" expand="block" onClick={handleFormSubmit}>Sign up</IonButton>

            <IonText className={styles.secondCTA} color="dark">Have an account? <IonRouterLink routerLink="/login">Log in</IonRouterLink></IonText>
          </div>
        </div>
        <IonLoading
          isOpen={showLoading}
          onDidDismiss={() => setShowLoading(false)}
          message={'Please wait...'}
          keyboard-close={true}
        />
        <IonAlert
          isOpen={showAlert}
          onDidDismiss={() => setShowAlert(false)}
          cssClass='my-custom-class'
          header={message}
          message={''}
          buttons={['OK']}
        />
      </IonContent>
    </IonPage>
  );
};

export default Signup;
