import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonText, IonTabButton, IonIcon, IonLabel, IonTabBar } from '@ionic/react';
import { search, personOutline, imageOutline } from 'ionicons/icons'
import ExploreContainer from '../components/ExploreContainer';
import styles from './Search.module.css';

const Search: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Search</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Search</IonTitle>
          </IonToolbar>
        </IonHeader>
        <ExploreContainer name="Search page coming soon™" />
      </IonContent>

      <IonTabBar slot="bottom" style={{border: 'none'}}>
        <IonTabButton tab="photos" href="/photos">
          <IonIcon icon={imageOutline} />
          <IonLabel>Photos</IonLabel>
        </IonTabButton>
        <IonTabButton tab="tab2" href="/search">
          <IonIcon icon={search} />
          <IonLabel>Search</IonLabel>
        </IonTabButton>
        <IonTabButton tab="account" href="/account">
          <IonIcon icon={personOutline} />
          <IonLabel>Account</IonLabel>
        </IonTabButton>
      </IonTabBar>
    </IonPage>
  );
};

export default Search;
