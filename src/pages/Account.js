import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonText, IonTabButton, IonIcon, IonLabel, IonTabBar, IonButton } from '@ionic/react';
import { search, personOutline, imageOutline } from 'ionicons/icons'
import { useHistory } from 'react-router-dom';
import styles from './Account.module.css';

const Account: React.FC = () => {
  const history = useHistory();

  const handleLogout = () => {
    localStorage.removeItem('user');
    history.push('/login');
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Account</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Account</IonTitle>
          </IonToolbar>
        </IonHeader>
        <div className={styles.contentWrapper}>
          <div className={styles.content}>
            <IonButton color="warning" onClick={handleLogout}>Logout</IonButton>
          </div>
        </div>
      </IonContent>

      <IonTabBar slot="bottom" style={{border: 'none'}}>
        <IonTabButton tab="photos" href="/photos">
          <IonIcon icon={imageOutline} />
          <IonLabel>Photos</IonLabel>
        </IonTabButton>
        <IonTabButton tab="tab2" href="/search">
          <IonIcon icon={search} />
          <IonLabel>Search</IonLabel>
        </IonTabButton>
        <IonTabButton tab="account" href="/account">
          <IonIcon icon={personOutline} />
          <IonLabel>Account</IonLabel>
        </IonTabButton>
      </IonTabBar>
    </IonPage>
  );
};

export default Account;
