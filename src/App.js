import { Redirect, Route } from 'react-router-dom';
import {
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
} from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';

import Login from './pages/Login';
import Signup from './pages/Signup';
import Photos from './pages/Photos';
import Search from './pages/Search';
import Account from './pages/Account';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';

/* Configure Ionic to use IOS components*/
import {setupConfig} from '@ionic/react'

setupConfig({
  mode: 'ios',
});


const App: React.FC = () => {
  const user = localStorage.getItem('user');
  const mainRedirect = user ? '/photos' : '/signup';

  return (
    <IonApp>
      <IonReactRouter>
        <IonRouterOutlet>
          <Route exact path="/signup">
            <Signup />
          </Route>
    		  <Route exact path="/login">
            <Login />
          </Route>
          <Route exact path="/photos">
            <Photos />
          </Route>
          <Route exact path="/search">
            <Search />
          </Route>
          <Route path="/account">
            <Account />
          </Route>
          <Route exact path="/">
            <Redirect to={mainRedirect} />
          </Route>
        </IonRouterOutlet>
      </IonReactRouter>
    </IonApp>
  );
};

export default App;
