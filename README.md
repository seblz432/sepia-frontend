Looking for the [source code of the website](https://gitlab.com/seblz432/sepia-landing-page)?

## Welcome to the source code for Sepia's apps!

In an effort to be as transparent as possible, we have released the source code for our app. All of our apps which includes the Android, iOS, and web app, use the same codebase you see here.

**Please note** this source code is provided under the [GNU AGPLv3](https://choosealicense.com/licenses/agpl-3.0/) licence and so before using this code for anything other than personal use I recommend familiarizing yourself with what that license entails.

In its current state, the app does not yet interface with any sort of backend and is just a start towards a functional MVP (minimum viable product).

Once we build out the backend, we will start making changes here to interface with it.


## Below you will find instructions and resources

This is an [Ionic](https://ionicframework.com/) app bootstrapped with [`ionic start myApp`]

> Note: If you're not familiar with the Ionic frameowork it might be helpful to go ober the [tutorial guide](https://ionicframework.com/docs/react/your-first-app),

0.  Install Ionic if needed: `npm install -g @ionic/cli`.
1.  Clone this repository.
2.  In a terminal, change directory into the repo: `cd sepia-frontend`.
3.  Install all packages: `npm install`.
4.  Run on the web: `ionic serve`.
5.  Run on iOS or Android: See [here](https://ionicframework.com/docs/building/running).
